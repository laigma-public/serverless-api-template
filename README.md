<img src="./assets/lp-logo.png" alt="Ligma Prods" height="60"/>
<img src="https://user-images.githubusercontent.com/2752551/30404911-d575539c-989d-11e7-9a74-df8533b95c6d.png" alt="serverless" height="60"/>

# Serverless Api Node JS

## Requisitos
- node 14.x o superior
- npm

## Instrucciones:

### 1- Instalar dependencias
 ```
 npm install
 ```

### 2- Ejecutar localmente
 ```
 npm run dev
 ```

### 3- Desplegar
 ```
 serverless deploy
 ```
 

### Ejecutar localmente

Para emular localmente DynamoDB, API Gateway y Lambda mediante los plugins `serverless-dynamodb-local` y `serverless-offline`, ejecuta:

```bash
serverless plugin install -n serverless-dynamodb-local
serverless plugin install -n serverless-offline
```

```bash
serverless offline start
```
